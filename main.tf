# 1. Create two different VPC's with below services using terraform template along with peering
# VPC
# Subnet(private and public)
# igw
# nat   	
# rt
# 2. Write a terraform to Create a EC2 instance with user_data script to install jenkins
# 3. Install SSH plugin and enable connection to EC2 instance using ec2 plugin
# 4. Configure a Jenkins worker node with ec2 which can only provision respective job matches the label of slave node
# once job execution done instance suppose to terminate automatically.
# 5. Create instance in another VPC private subnet with install mysql using user_data script
# 6. Try to connect mysql private endpoint from another vpc instance, it should work as we enabled peering.
# 7. Do some admin operations
# - Reset the Jenkins password using jenkins configuration.
# - Pull the jenkins logs
# - uninstall and install new plugins
# - job console ouput verification
# 8. Configure a job which can trigger whenever some changes push to gitrepo(configure webhook with repo)
#
# ============================================================================================================================


#Creating Providers 
provider "aws"{
    region = "ap-south-1"
}

# Creating two VPC's
resource "aws_vpc" "vpc1" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "vpc1"
  }
}

resource "aws_vpc" "vpc2" {
  cidr_block = "192.168.0.0/16"
  tags = {
    Name = "vpc2"
  }
}

# Creating Subnets for vpc1 and vpc2
resource "aws_subnet" "subnet1" {
  vpc_id     = aws_vpc.vpc1.id
  cidr_block = "10.0.1.0/24"
  
  tags = {
    Name = "v1sub1"
  }
}

resource "aws_subnet" "subnet2" {
  vpc_id     = aws_vpc.vpc2.id
  cidr_block = "192.168.1.0/24"

  tags = {
    Name = "v1sub2"
  }
}

# Creating Subnets Associaton with Route Table

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.subnet1.id
  route_table_id = aws_route_table.v1routetable.id
}

# Creating IGW 
resource "aws_internet_gateway" "v1gw" {
  vpc_id = aws_vpc.vpc1.id

  tags = {
    Name = "v1gw"
  }
}

# creating NAT for public and private subnets

# resource "aws_nat_gateway" "v1nat" {
#   subnet_id     = aws_subnet.subnet1.id

#   tags = {
#     Name = "v1gw NAT"
#   }

#   # To ensure proper ordering, it is recommended to add an explicit dependency
#   # on the Internet Gateway for the VPC.
#   depends_on = [aws_internet_gateway.v1gw]
# }

resource "aws_nat_gateway" "v1privatenat" {
  connectivity_type = "private"
  subnet_id         = aws_subnet.subnet2.id
}

# Creating Routing Table
resource "aws_route_table" "v1routetable" {
  vpc_id = aws_vpc.vpc1.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.v1gw.id
  }

  route {
    ipv6_cidr_block        = "::/0"
    gateway_id = aws_internet_gateway.v1gw.id
  }

  tags = {
    Name = "v1routetable"
  }
}


# Creating VPC Peering between two VPC's
resource "aws_vpc_peering_connection" "vpc12vpc2" {
  peer_owner_id = "218032717447"
  peer_vpc_id   = aws_vpc.vpc2.id
  vpc_id        = aws_vpc.vpc1.id
  auto_accept   = true

  tags = {
    Name = "VPC Peering between foo and bar"
  }
}


# Creating Network Interface 
# resource "aws_network_interface" "v1ni" {
#   subnet_id   = aws_subnet.subnet1.id
#   private_ips = ["10.0.1.9"]

#   tags = {
#     Name = "primary_network_interface"
#   }
# }

# Creating Security Group
resource "aws_security_group" "allow_tls" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.vpc1.id

  ingress {
    description      = "TLS from VPC"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = [aws_vpc.vpc1.cidr_block]
  }
  
  ingress {
    description      = "TLS from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = [aws_vpc.vpc1.cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}

resource "aws_network_interface_sg_attachment" "sg_attachment" {
  security_group_id    = aws_security_group.allow_tls.id
  network_interface_id = aws_network_interface.test1.id
}


resource "aws_network_interface" "test1" {
  subnet_id       = aws_subnet.subnet1.id
  private_ips     = ["10.0.1.10"]
  security_groups = [aws_security_group.allow_tls.id]

  attachment {
    instance     = aws_instance.jenkins.id
    device_index = 0
    
  }
}

# Write a terraform to Create a EC2 instance with user_data script to install jenkins
resource "aws_instance" "jenkins" {
  ami = "ami-0851b76e8b1bce90b"
  instance_type = "t2.micro"
  key_name = "terraform"
  associate_public_ip_address = true

  user_data = <<EOF
   #! /bin/bash
   sudo apt update -y
   sudo apt install fontconfig openjdk-11-jre -y
   curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo tee \
  /usr/share/keyrings/jenkins-keyring.asc > /dev/null
  echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
  https://pkg.jenkins.io/debian-stable binary/ | sudo tee \
  /etc/apt/sources.list.d/jenkins.list > /dev/null
  sudo apt-get update -y
  sudo apt-get install jenkins -y
  EOF
  tags = {
	Name = "Jenkins_ubuntu"
  }
}
